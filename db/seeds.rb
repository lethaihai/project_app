Place.create!([
                  { "name": "Công Ty TNHH Rikkeisoft Hà Nội", "latitude": "21.016492", "longitude": "105.779465"},
                  { "name": "Công Ty TNHH Rikkeisoft Đà Nẵng", "latitude": "16.074332", "longitude": "108.218613"}
              ])

# 1
User.create!(name: "Lê Thái Hải",
             email: "lethaihai666@gmail.com",
             role: "admin",
             date_of_birth: "15/01/1997",
             telephone: "0392892686",
             division: "D2",
             introduction: "Em là sinh viên trường Đại học Điện Lực.
                            Em muốn tìm hiểu và học về Ruby on Rails.",
             password: "seaside666",
             password_confirmation: "seaside666",
             deleted: false,
             activated: true,
             activated_at: Time.zone.now)

# 2
User.create!(name: "Trần Trọng Quang",
             email: "quangtt@rikkeisoft.com",
             role: "trainer",
             date_of_birth: "28/12/1991",
             telephone: "0388205950",
             division: "D2",
             introduction: "Tôi là thành viên của đội Ruby.
                            Tôi đã có nhiều năm kinh nghiệm làm việc
                            với Git, Ruby, Rails, PHP,...",
             password: "seaside666",
             password_confirmation: "seaside666",
             deleted: false,
             activated: true,
             activated_at: Time.zone.now)

# 3
User.create!(name: "Phan Thị Linh",
             email: "linhpt@rikkeisoft.com",
             role: "trainer",
             date_of_birth: "20/02/1992",
             telephone: "0363985621",
             division: "D6",
             introduction: "Tôi là thành viên của đội QA.
                            Tôi đã có nhiều năm kinh nghiệm làm việc
                            với Test, Test Automation, Java,...",
             password: "seaside666",
             password_confirmation: "seaside666",
             deleted: false,
             activated: true,
             activated_at: Time.zone.now)

# 4
User.create!(name: "Luyện Hữu Dương",
             email: "duonglh@rikkeisoft.com",
             role: "trainer",
             date_of_birth: "3/10/1989",
             telephone: "0984362214",
             division: "D2",
             introduction: "Tôi là thành viên của đội PHP.
                            Tôi đã có nhiều năm kinh nghiệm làm việc
                            với Git, PHP, Laravel,...",
             password: "seaside666",
             password_confirmation: "seaside666",
             deleted: false,
             activated: true,
             activated_at: Time.zone.now)

# 5
User.create!(name: "Đỗ Xuân Kính",
             email: "kinhdx@rikkeisoft.com",
             role: "trainer",
             date_of_birth: "09/11/1985",
             telephone: "0904652873",
             division: "D6",
             introduction: "Tôi là thành viên của đội Java.
                            Tôi đã có nhiều năm kinh nghiệm làm việc
                            với Git, Java, Android,...",
             password: "seaside666",
             password_confirmation: "seaside666",
             deleted: false,
             activated: true,
             activated_at: Time.zone.now)

# 6
User.create!(name: "Phạm Ngọc Chuyển",
             email: "chuyenpn@rikkeisoft.com",
             role: "trainer",
             date_of_birth: "04/07/1992",
             telephone: "0976325781",
             division: "D6",
             introduction: "Tôi là thành viên của đội Frontend.
                            Tôi đã có nhiều năm kinh nghiệm làm việc
                            với Git, Javascript, HTML, CSS,...",
             password: "seaside666",
             password_confirmation: "seaside666",
             deleted: false,
             activated: true,
             activated_at: Time.zone.now)

# 7
User.create!(name: "Trịnh Xuân Hiếu",
             email: "hieutx@rikkeisoft.com",
             role: "trainer",
             date_of_birth: "18/08/1990",
             telephone: "0848772541",
             division: "D5",
             introduction: "Tôi là thành viên của đội C#.
                            Tôi đã có nhiều năm kinh nghiệm làm việc
                            với Git, C#, .NET,...",
             password: "seaside666",
             password_confirmation: "seaside666",
             deleted: false,
             activated: true,
             activated_at: Time.zone.now)

# 8
User.create!(name: "Vũ Đình Long",
             email: "longvd@rikkeisoft.com",
             role: "trainer",
             date_of_birth: "19/03/1983",
             telephone: "0892657843",
             division: "D5",
             introduction: "Tôi là thành viên của đội C/C++.
                            Tôi đã có nhiều năm kinh nghiệm làm việc
                            với Git, C, C++, Java,...",
             password: "seaside666",
             password_confirmation: "seaside666",
             deleted: false,
             activated: true,
             activated_at: Time.zone.now)

# 9
User.create!(name: "Đỗ Đức Mạnh",
             email: "manhdd@rikkeisoft.com",
             role: "trainer",
             date_of_birth: "17/04/1987",
             telephone: "0346982135",
             division: "D6",
             introduction: "Tôi là thành viên của đội Java.
                            Tôi đã có nhiều năm kinh nghiệm làm việc
                            với Git, Java, Android,...",
             password: "seaside666",
             password_confirmation: "seaside666",
             deleted: false,
             activated: true,
             activated_at: Time.zone.now)

# 10
User.create!(name: "Lê Văn Kiên",
             email: "kienlv@rikkeisoft.com",
             role: "trainer",
             date_of_birth: "11/01/1987",
             telephone: "0367894256",
             division: "D1",
             introduction: "Tôi là thành viên của đội Mobile.
                            Tôi đã có nhiều năm kinh nghiệm làm việc
                            với Git, IOS, Android, React-Native,...",
             password: "seaside666",
             password_confirmation: "seaside666",
             deleted: false,
             activated: true,
             activated_at: Time.zone.now)

# 11
User.create!(name: "Trịnh Minh Dũng",
             email: "dungtm@rikkeisoft.com",
             role: "trainer",
             date_of_birth: "01/01/1982",
             telephone: "0973777325",
             division: "D2",
             introduction: "Tôi là đội trưởng của đội Ruby.
                            Tôi đã có nhiều năm kinh nghiệm làm việc
                            với Git, Ruby, Rails, Angular,...",
             password: "seaside666",
             password_confirmation: "seaside666",
             deleted: false,
             activated: true,
             activated_at: Time.zone.now)

# 12
User.create!(name: "Hoàng Văn Kỳ",
             email: "kyhv@rikkeisoft.com",
             role: "trainer",
             date_of_birth: "15/09/1988",
             telephone: "0367894256",
             division: "D1",
             introduction: "Tôi là thành viên của đội Mobile.
                            Tôi đã có nhiều năm kinh nghiệm làm việc
                            với Git, IOS, Android, React-Native,...",
             password: "seaside666",
             password_confirmation: "seaside666",
             deleted: false,
             activated: true,
             activated_at: Time.zone.now)

# 13
User.create!(name: "Nguyễn Đình Khánh",
             email: "khanhnd@rikkeisoft.com",
             role: "trainer",
             date_of_birth: "25/06/1990",
             telephone: "0985632490",
             division: "D5",
             introduction: "Tôi là thành viên của đội C#.
                            Tôi đã có nhiều năm kinh nghiệm làm việc
                            với Git, C#, .NET, Unity...",
             password: "seaside666",
             password_confirmation: "seaside666",
             deleted: false,
             activated: true,
             activated_at: Time.zone.now)

# 14 - 2
User.create!(name: "Nguyễn Ngọc Duy",
             email: "duynn.mta@gmail.com",
             role: "trainee",
             date_of_birth: "07/03/1997",
             telephone: "0962297397",
             division: "D2",
             introduction: "Em là sinh viên trường Học viện ký thuật quân sự.
                            Em muốn tìm hiểu và học về Ruby on Rails",
             password: "seaside666",
             password_confirmation: "seaside666",
             deleted: false,
             activated: true,
             activated_at: Time.zone.now)

# 15 - 3
User.create!(name: "Vũ Thị Ngọc Ánh",
             email: "anhvu010597@gmail.com",
             role: "trainee",
             date_of_birth: "05/01/1997",
             telephone: "0353772561",
             division: "D6",
             introduction: "Em là sinh viên trường Đại học Điện Lực.
                            Em muốn tìm hiểu và học về Test, QA",
             password: "seaside666",
             password_confirmation: "seaside666",
             deleted: false,
             activated: true,
             activated_at: Time.zone.now)

# 16 - 4
User.create!(name: "Kiều Quang Giáp",
             email: "aekhopro97@gmail.com",
             role: "trainee",
             date_of_birth: "08/01/1997",
             telephone: "0365431834",
             division: "D6",
             introduction: "Em là sinh viên trường Đại học Điện Lực.
                            Em muốn tìm hiểu và học về PHP",
             password: "seaside666",
             password_confirmation: "seaside666",
             deleted: false,
             activated: true,
             activated_at: Time.zone.now)

# 17 - 4
User.create!(name: "Nguyễn Duy Cường",
             email: "duycuong1297@gmail.com",
             role: "trainee",
             date_of_birth: "12/11/1997",
             telephone: "0384715726",
             division: "D6",
             introduction: "Em là sinh viên trường Đại học Điện Lực.
                            Em muốn tìm hiểu và học về PHP",
             password: "seaside666",
             password_confirmation: "seaside666",
             deleted: false,
             activated: true,
             activated_at: Time.zone.now)

# 18 - 4
User.create!(name: "Trần Duy Long",
             email: "tranduylong02091997@gmail.com",
             role: "trainee",
             date_of_birth: "02/09/1997",
             telephone: "0965397494",
             division: "D2",
             introduction: "Em là sinh viên trường Đại học Điện Lực.
                            Em muốn tìm hiểu và học về PHP",
             password: "seaside666",
             password_confirmation: "seaside666",
             deleted: false,
             activated: true,
             activated_at: Time.zone.now)

# 19 - 4
User.create!(name: "Nguyễn Thị Ngọc",
             email: "ngocnguyents5@gmail.com",
             role: "trainee",
             date_of_birth: "05/03/1997",
             telephone: "0981792340",
             division: "D2",
             introduction: "Em là sinh viên trường Đại học Điện Lực.
                            Em muốn tìm hiểu và học về PHP",
             password: "seaside666",
             password_confirmation: "seaside666",
             deleted: false,
             activated: true,
             activated_at: Time.zone.now)

# 20 - 4
User.create!(name: "Nguyễn Hữu Uyển",
             email: "nguyenhuuuyen123@gmail.com",
             role: "trainee",
             date_of_birth: "09/03/1997",
             telephone: "0971699134",
             division: "D5",
             introduction: "Em là sinh viên trường Đại học Điện Lực.
                            Em muốn tìm hiểu và học về PHP",
             password: "seaside666",
             password_confirmation: "seaside666",
             deleted: false,
             activated: true,
             activated_at: Time.zone.now)

# 21 - 5
User.create!(name: "Nguyễn Chí Linh",
             email: "chilinhnguyennb96@gmail.com",
             role: "trainee",
             date_of_birth: "02/12/1996",
             telephone: "0973897473",
             division: "D6",
             introduction: "Em là sinh viên trường Đại học Điện Lực.
                            Em muốn tìm hiểu và học về Java",
             password: "seaside666",
             password_confirmation: "seaside666",
             deleted: false,
             activated: true,
             activated_at: Time.zone.now)

# 22 - 6
User.create!(name: "Phạm Thế Bách",
             email: "phamthebach2210@gmail.com",
             role: "trainee",
             date_of_birth: "22/10/1997",
             telephone: "0965737997",
             division: "D6",
             introduction: "Em là sinh viên trường Đại học Điện Lực.
                            Em muốn tìm hiểu và học về Frontend",
             password: "seaside666",
             password_confirmation: "seaside666",
             deleted: false,
             activated: true,
             activated_at: Time.zone.now)

# 23- 6
User.create!(name: "Trần Thị Hà",
             email: "hatran07092015@gmail.com",
             role: "trainee",
             date_of_birth: "13/03/1997",
             telephone: "0336344069",
             division: "D6",
             introduction: "Em là sinh viên trường Đại học Điện Lực.
                            Em muốn tìm hiểu và học về Frontend",
             password: "seaside666",
             password_confirmation: "seaside666",
             deleted: false,
             activated: true,
             activated_at: Time.zone.now)

# 24- 6
User.create!(name: "Hoàng Đức Duy",
             email: "bomduy1997@gmail.com",
             role: "trainee",
             date_of_birth: "03/09/1997",
             telephone: "0975206897",
             division: "D6",
             introduction: "Em là sinh viên trường Đại học Điện Lực.
                            Em muốn tìm hiểu và học về Frontend",
             password: "seaside666",
             password_confirmation: "seaside666",
             deleted: false,
             activated: true,
             activated_at: Time.zone.now)

# 25- 6
User.create!(name: "Vũ Văn Nam",
             email: "namvuhc12345@gmail.com",
             role: "trainee",
             date_of_birth: "20/11/1997",
             telephone: "0335236018",
             division: "D6",
             introduction: "Em là sinh viên trường Đại học Điện Lực.
                            Em muốn tìm hiểu và học về Frontend",
             password: "seaside666",
             password_confirmation: "seaside666",
             deleted: false,
             activated: true,
             activated_at: Time.zone.now)

# 26 - 7
User.create!(name: "Vũ Thế Linh",
             email: "vuthelinh123@gmail.com",
             role: "trainee",
             date_of_birth: "19/05/1997",
             telephone: "0964680069",
             division: "D5",
             introduction: "Em là sinh viên trường Đại học Điện Lực.
                            Em muốn tìm hiểu và học về .NET",
             password: "seaside666",
             password_confirmation: "seaside666",
             deleted: false,
             activated: true,
             activated_at: Time.zone.now)

# 27 - 8
User.create!(name: "Lê Hoàng Sơn",
             email: "lehoangson16@gmail.com",
             role: "trainee",
             date_of_birth: "22/08/1995",
             telephone: "0981115693",
             division: "D5",
             introduction: "Em là sinh viên trường Đại học Điện Lực.
                            Em muốn tìm hiểu và học về C/C++",
             password: "seaside666",
             password_confirmation: "seaside666",
             deleted: false,
             activated: true,
             activated_at: Time.zone.now)

# 28 - 9
User.create!(name: "Hồ Xuân Hoàng Dương",
             email: "hxhduong4394@gmail.com",
             role: "trainee",
             date_of_birth: "04/03/1994",
             telephone: "0978901454",
             division: "D5",
             introduction: "Em là sinh viên trường Đại học Mỏ Địa Chất.
                            Em muốn tìm hiểu và học về Java",
             password: "seaside666",
             password_confirmation: "seaside666",
             deleted: false,
             activated: true,
             activated_at: Time.zone.now)

# 29 - 10
User.create!(name: "Nguyễn Văn Tú",
             email: "nguyenvantu.epu@gmail.com",
             role: "trainee",
             date_of_birth: "19/04/1996",
             telephone: "0942095232",
             division: "D1",
             introduction: "Em là sinh viên trường Đại học Điện Lực.
                            Em muốn tìm hiểu và học về React-Native",
             password: "seaside666",
             password_confirmation: "seaside666",
             deleted: false,
             activated: true,
             activated_at: Time.zone.now)

# 30 - 11
User.create!(name: "Đặng Xuân Phúc",
             email: "dangxuanphuc2211@gmail.com",
             role: "trainee",
             date_of_birth: "12/12/1998",
             telephone: "0981658389",
             division: "D2",
             introduction: "Em là sinh viên trường Đại học Khoa học Tự Nhiên – ĐHQGHN.
                            Em muốn tìm hiểu và học về Ruby on Rails",
             password: "seaside666",
             password_confirmation: "seaside666",
             deleted: false,
             activated: true,
             activated_at: Time.zone.now)

# 31 - 12
User.create!(name: "Vũ Quốc Trọng",
             email: "vuquoctrong141@gmail.com",
             role: "trainee",
             date_of_birth: "14/01/1996",
             telephone: "0396598517",
             division: "D1",
             introduction: "Em là sinh viên trường Học Viện Công Nghệ Bưu Chính Viễn Thông.
                            Em muốn tìm hiểu và học về Android",
             password: "seaside666",
             password_confirmation: "seaside666",
             deleted: false,
             activated: true,
             activated_at: Time.zone.now)

# 32 - 12
User.create!(name: "Vũ Nhật Bình",
             email: "nbbn0204@gmail.com",
             role: "trainee",
             date_of_birth: "09/09/1996",
             telephone: "0362128225",
             division: "D1",
             introduction: "Em là sinh viên trường Đại học Kinh doanh và Công nghệ.
                            Em muốn tìm hiểu và học về Android",
             password: "seaside666",
             password_confirmation: "seaside666",
             deleted: false,
             activated: true,
             activated_at: Time.zone.now)

# 33 - 13
User.create!(name: "Nguyễn Ngọc Quý",
             email: "quynguyenngoc1996@gmail.com",
             role: "trainee",
             date_of_birth: "26/06/1996",
             telephone: "0833463894",
             division: "D3",
             introduction: "Em là sinh viên trường Đại học FPT.
                            Em muốn tìm hiểu và học về Unity",
             password: "seaside666",
             password_confirmation: "seaside666",
             deleted: false,
             activated: true,
             activated_at: Time.zone.now)

# 34 - 7
User.create!(name: "Hán Đình Đôn",
             email: "handinhdon@gmail.com",
             role: "trainee",
             date_of_birth: "23/10/1996",
             telephone: "0968447492",
             division: "D5",
             introduction: "Em là sinh viên trường Đại học Điện Lực.
                            Em muốn tìm hiểu và học về .NET",
             password: "seaside666",
             password_confirmation: "seaside666",
             deleted: false,
             activated: true,
             activated_at: Time.zone.now)

# 35 - 8
User.create!(name: "Nguyễn Hoàng Minh",
             email: "hoangminh271095@gmail.com",
             role: "trainee",
             date_of_birth: "27/10/1995",
             telephone: "0985166817",
             division: "D5",
             introduction: "Em là sinh viên trường Học viện Nông nghiệp Việt Nam.
                            Em muốn tìm hiểu và học về C/C++",
             password: "seaside666",
             password_confirmation: "seaside666",
             deleted: false,
             activated: true,
             activated_at: Time.zone.now)

# 36 - 3
User.create!(name: "Trịnh Thúy Hằng",
             email: "thuyhang2396@gmail.com",
             role: "trainee",
             date_of_birth: "02/03/1996",
             telephone: "0379766721",
             division: "D6",
             introduction: "Em là sinh viên trường Đại học Lao động - Xã hội.
                            Em muốn tìm hiểu và học về Test, QA",
             password: "seaside666",
             password_confirmation: "seaside666",
             deleted: false,
             activated: true,
             activated_at: Time.zone.now)

# ---------------Projects----------------

# 1 - 2
Project.create!(name: "Shopping Cart Website",
                trainer_id: "2",
                duration: "1 tháng",
                description: "Đây là một project để training Ruby on Rails.
                              Bạn sẽ xây dựng một website bán hàng cùng với
                              giỏ hàng và học cách sử dụng git, rubymine,...",
                deleted: false
                )

# 2 - 2
Project.create!(name: "Microposts Website",
                trainer_id: "2",
                duration: "1 tháng",
                description: "Đây là một project để training Ruby on Rails.
                              Bạn sẽ xây dựng một website giống như twitter
                              và học cách sử dụng git, rubymine,...",
                deleted: false
                )

# 3 - 2
Project.create!(name: "Blogs Website",
                trainer_id: "2",
                duration: "1 tháng",
                description: "Đây là một project để training Ruby on Rails.
                              Bạn sẽ xây dựng một website blog đơn giản
                              và học cách sử dụng git, rubymine,...",
                deleted: false
                )

# 4 - 3
Project.create!(name: "Test Clothes App",
                trainer_id: "3",
                duration: "1 tháng",
                description: "Đây là một project để training Test, QA.
                              Bạn sẽ test một phần mềm bán quần áo, trang phục
                              và học cách viết testcase chuẩn bằng excel.",
                deleted: false
                )

# 5 - 3
Project.create!(name: "Test Blogs Website",
                trainer_id: "3",
                duration: "1 tháng",
                description: "Đây là một project để training Test, QA
                              Bạn sẽ test một website blog đơn giản
                              và học cách viết testcase chuẩn bằng excel.",
                deleted: false
                )

# 6 - 3
Project.create!(name: "Test News Website",
                trainer_id: "3",
                duration: "1 tháng",
                description: "Đây là một project để training Test, QA.
                              Bạn sẽ test một website tin tức, hình ảnh
                              và học cách viết testcase chuẩn bằng excel.",
                deleted: false
                )

# 7 - 4
Project.create!(name: "Shoes Website",
                trainer_id: "4",
                duration: "1 tháng",
                description: "Đây là một project để training PHP, Laravel.
                              Bạn sẽ xây dựng một website bán giày
                              và học cách sử dụng git, phpstorm,...",
                deleted: false
)

# 8 - 4
Project.create!(name: "MyBlog Website",
                trainer_id: "4",
                duration: "1 tháng",
                description: "Đây là một project để training PHP, Laravel.
                              Bạn sẽ xây dựng một website blog đơn giản
                              và học cách sử dụng git, phpstorm,...",
                deleted: false
)

# 9 - 5
Project.create!(name: "Mobile Store Website",
                trainer_id: "5",
                duration: "1 tháng",
                description: "Đây là một project để training Java.
                              Bạn sẽ xây dựng một website bán điện thoại
                              và học cách sử dụng git, netbeans,...",
                deleted: false
)

# 10 - 5
Project.create!(name: "Staff App",
                trainer_id: "5",
                duration: "1 tháng",
                description: "Đây là một project để training Java.
                              Bạn sẽ xây dựng một phần mềm quản lý nhân viên
                              và học cách sử dụng git, netbeans,...",
                deleted: false
)

# 11 - 6
Project.create!(name: "Staff Website",
                trainer_id: "6",
                duration: "1 tháng",
                description: "Đây là một project để training Frontend.
                              Bạn sẽ xây dựng giao diện website quản lý nhân
                              viên và học cách sử dụng git, sublime text,...",
                deleted: false
)

# 12 - 6
Project.create!(name: "Store Website",
                trainer_id: "6",
                duration: "1 tháng",
                description: "Đây là một project để training Frontend.
                              Bạn sẽ xây dựng giao diện website bán hàng
                              và học cách sử dụng git, sublime text,...",
                deleted: false
)

# 13 - 7
Project.create!(name: "Audio Store App",
                trainer_id: "7",
                duration: "1 tháng",
                description: "Đây là một project để training C#.
                              Bạn sẽ xây dựng phần mềm bán tai nghe, loa
                              và học cách sử dụng git, visual studio,...",
                deleted: false
)

# 14 - 7
Project.create!(name: "Car Store App",
                trainer_id: "7",
                duration: "1 tháng",
                description: "Đây là một project để training C#.
                              Bạn sẽ xây dựng một ứng dụng giới thiệu, bán ô tô
                              và học cách sử dụng git, visual studio,...",
                deleted: false
)

# 15 - 8
Project.create!(name: "Weather App",
                trainer_id: "8",
                duration: "1 tháng",
                description: "Đây là một project để training C/C++.
                              Bạn sẽ xây dựng một phần mềm dự báo thời tiết
                              và học cách sử dụng git, visual studio,...",
                deleted: false
)

# 16 - 8
Project.create!(name: "Fingerprint App",
                trainer_id: "8",
                duration: "1 tháng",
                description: "Đây là một project để training C/C++.
                              Bạn sẽ xây dựng một phần mềm nhận diện vân tay
                              và học cách sử dụng git, visual studio,...",
                deleted: false
)

# 17 - 9
Project.create!(name: "Student App",
                trainer_id: "9",
                duration: "1 tháng",
                description: "Đây là một project để training Java.
                              Bạn sẽ xây dựng một phần mềm quản lý sinh viên
                              và học cách sử dụng git, netbeans,...",
                deleted: false
)

# 18 - 9
Project.create!(name: "Motobike Website",
                trainer_id: "9",
                duration: "1 tháng",
                description: "Đây là một project để training Java.
                              Bạn sẽ xây dựng một website giới thiệu, bán xe máy
                              và học cách sử dụng git, netbeans,...",
                deleted: false
)

# 19 - 10
Project.create!(name: "Shopping App",
                trainer_id: "10",
                duration: "1 tháng",
                description: "Đây là một project để training React-Native.
                              Bạn sẽ xây dựng một ứng dụng mua sắm trên điện thoại
                              và học cách sử dụng git, android studio,...",
                deleted: false
)

# 20 - 10
Project.create!(name: "News App",
                trainer_id: "10",
                duration: "1 tháng",
                description: "Đây là một project để training React-Native.
                              Bạn sẽ xây dựng một ứng dụng tin tức trên điện thoại
                              và học cách sử dụng git, android studio,...",
                deleted: false
)

# # 21 - 11
# Project.create!(name: "Movies Website",
#                 trainer_id: "11",
#                 duration: "1 tháng",
#                 description: "Đây là một project để training Ruby on Rails.
#                               Bạn sẽ xây dựng một website xem phim online
#                               và học cách sử dụng git, rubymine,...",
#                 deleted: false
# )
#
# # 22 - 12
# Project.create!(name: "Travel App",
#                 trainer_id: "12",
#                 duration: "1 tháng",
#                 description: "Đây là một project để training Android.
#                               Bạn sẽ xây dựng một ứng dụng du lịch trên điện thoại
#                               và học cách sử dụng git, android studio,...",
#                 deleted: false
# )
#
# # 23 - 12
# Project.create!(name: "2048 Game",
#                 trainer_id: "12",
#                 duration: "1 tháng",
#                 description: "Đây là một project để training IOS.
#                               Bạn sẽ xây dựng một trò chơi 2048 điện thoại
#                               và học cách sử dụng git, switch,...",
#                 deleted: false
# )
#
# # 24 - 13
# Project.create!(name: "Filter Picture App",
#                 trainer_id: "13",
#                 duration: "1 tháng",
#                 description: "Đây là một project để training C#.
#                               Bạn sẽ xây dựng một ứng dụng lọc, xử lý ảnh
#                               và học cách sử dụng git, visual studio,...",
#                 deleted: false
# )
#
# # 25 - 13
# Project.create!(name: "Snake Game",
#                 trainer_id: "13",
#                 duration: "1 tháng",
#                 description: "Đây là một project để training Unity.
#                               Bạn sẽ xây dựng một trò chơi rắn săn mồi
#                               và học cách sử dụng git, visual studio,...",
#                 deleted: false
# )

# ---------------Users_Projects----------------

# 1 - 1 - 14
UsersProject.create!(trainee_id: "14",
                     project_id: "1",
                     started: "",
                     finished: "",
                     status: "")

# 2 - 5 - 15
UsersProject.create!(trainee_id: "15",
                     project_id: "5",
                     started: "",
                     finished: "",
                     status: "")

# 3 - 6 - 36
UsersProject.create!(trainee_id: "19",
                     project_id: "1",
                     started: "",
                     finished: "",
                     status: "")

# 4 - 7 - 16
UsersProject.create!(trainee_id: "16",
                     project_id: "7",
                     started: "",
                     finished: "",
                     status: "")

# 5 - 7 - 17
UsersProject.create!(trainee_id: "17",
                     project_id: "7",
                     started: "",
                     finished: "",
                     status: "")

# 6 - 8 - 18
UsersProject.create!(trainee_id: "18",
                     project_id: "8",
                     started: "",
                     finished: "",
                     status: "")

# 7 - 9 - 21
UsersProject.create!(trainee_id: "21",
                     project_id: "9",
                     started: "",
                     finished: "",
                     status: "")

# 8 - 11 - 22
UsersProject.create!(trainee_id: "22",
                     project_id: "11",
                     started: "",
                     finished: "",
                     status: "")

# 9 - 11 - 24
UsersProject.create!(trainee_id: "24",
                     project_id: "11",
                     started: "",
                     finished: "",
                     status: "")

# 10 - 13 - 26
UsersProject.create!(trainee_id: "26",
                     project_id: "13",
                     started: "",
                     finished: "",
                     status: "")

# -----------Tasks-------------

# 1 - P
Task.create!(name: "Đọc tài liệu hướng dẫn và cài đặt môi trường, công cụ",
             duration: "1 ngày",
             description: "Bạn sẽ đọc tài liệu hướng dẫn thực tập theo link của
             người hướng dân gửi và cài đặt các môi trường, công cụ cần thiết để
             chuẩn bị thực hiện dự án.",
             deleted: false)

# 2 - P
Task.create!(name: "Học cách sử dụng Git, Gitlab",
             duration: "2 ngày",
             description: "Bạn sẽ tìm hiểu và thực hiện các thao tác cơ bản với
             Git: https://backlog.com/git-tutorial/vn/intro/intro1_1.html và
             Gitlab: https://gitlab.com",
             deleted: false)

# 3 - P1-2-3
Task.create!(name: "Tìm hiểu Ruby on Rails",
             duration: "7 ngày",
             description: "Bạn sẽ tìm hiểu và thực hiện các thao tác với
             biến, câu lệnh, hàm, phương thức, đối tượng trong Ruby on Rails.",
             deleted: false)

# 4 - P1-2-3
Task.create!(name: "Kiểm tra kiến thức",
             duration: "1 ngày",
             description: "Bạn sẽ được người hướng dẫn kiểm tra về kiến thức
             và các thao tác đã được học, tìm hiểu, thực hành.",
             deleted: false)

# 5 - P1-2-3
Task.create!(name: "Tìm hiểu, thực hiện dự án",
             duration: "18 ngày",
             description: "Bạn sẽ tìm hiểu và thực hiện dự án do người hướng
             dẫn giao. Trong quá trình thực hiện bạn cần phải làm theo yêu cầu,
             có thể tham khảo, trao đổi ý kiến với người hướng dẫn.",
             deleted: false)

# 6 - P
Task.create!(name: "Tổng kết và đánh giá kết quả",
             duration: "1 ngày",
             description: "Bạn sẽ được người hướng dẫn chấm điểm và đánh giá kết
             quả của dự án đã thực hiện và quá trình thực tập tại công ty.",
             deleted: false)

# 7 P7-8
Task.create!(name: "Tìm hiểu PHP Laravel",
             duration: "7 ngày",
             description: "Bạn sẽ tìm hiểu và thực hiện các thao tác với
             biến, câu lệnh, hàm, phương thức, đối tượng trong PHP Laravel.",
             deleted: false)

# 8 P4-5
Task.create!(name: "Tìm hiểu Test",
             duration: "7 ngày",
             description: "Bạn sẽ tìm hiểu và thực hiện các yêu cầu test,
             viết testcase, cách test,...",
             deleted: false)

# 9 P9-10
Task.create!(name: "Tìm hiểu Java",
             duration: "7 ngày",
             description: "Bạn sẽ tìm hiểu và thực hiện các thao tác với
             biến, câu lệnh, hàm, phương thức, đối tượng trong Java.",
             deleted: false)

# 10 P11-12
Task.create!(name: "Tìm hiểu Frontend",
             duration: "7 ngày",
             description: "Bạn sẽ tìm hiểu và thực hiện các thao tác với
             biến, câu lệnh, hàm, phương thức, đối tượng trong HTML, CSS, JS.",
             deleted: false)

# -----------Projects_Tasks------------

# 1 P1-T1
ProjectsTask.create!(project_id: "1",
                     task_id: "1",
                     parent_task_id: "0")

# 2 P1-T2
ProjectsTask.create!(project_id: "1",
                     task_id: "2",
                     parent_task_id: "1")

# 3 P1-T3
ProjectsTask.create!(project_id: "1",
                     task_id: "3",
                     parent_task_id: "2")

# 4 P1-T4
ProjectsTask.create!(project_id: "1",
                     task_id: "4",
                     parent_task_id: "3")

# 5 P1-T5
ProjectsTask.create!(project_id: "1",
                     task_id: "5",
                     parent_task_id: "4")

# 6 P1-T6
ProjectsTask.create!(project_id: "1",
                     task_id: "6",
                     parent_task_id: "5")

# 7 P7-T1
ProjectsTask.create!(project_id: "7",
                     task_id: "1",
                     parent_task_id: "0")

# 8 P7-T2
ProjectsTask.create!(project_id: "7",
                     task_id: "2",
                     parent_task_id: "7")

# 9 P7-T7
ProjectsTask.create!(project_id: "7",
                     task_id: "7",
                     parent_task_id: "8")

# 10 P7-T4
ProjectsTask.create!(project_id: "7",
                     task_id: "4",
                     parent_task_id: "9")

# --------------Evaluation---------------

# 1
Evaluation.create!(trainer_id: "2",
                   users_project_id: "1",
                   title: "Đánh giá thực tập sinh Nguyễn Ngọc Duy",
                   working_result: "Tốt",
                   working_attitude: "Nghiêm túc, chăm chỉ",
                   manners: "Đi về đúng giờ, nghỉ có xin phép",
                   creation: "Khá sáng tạo")

# 2
Evaluation.create!(trainer_id: "2",
                   users_project_id: "3",
                   title: "Đánh giá thực tập sinh Nguyễn Thị Ngọc",
                   working_result: "Khá",
                   working_attitude: " Tương đối nghiêm túc, chăm chỉ",
                   manners: "Nghỉ có xin phép, thực hiện đúng nội quy của công ty",
                   creation: "Có chút sáng tạo")

# 3
Evaluation.create!(trainer_id: "4",
                   users_project_id: "4",
                   title: "Đánh giá thực tập sinh Kiều Quang Giáp",
                   working_result: "Tương đối khá",
                   working_attitude: "Chưa thật sự nghiêm túc, chăm chỉ",
                   manners: "Thực hiện nội quy của công ty chưa thật sự tốt",
                   creation: "Không sáng tạo")

# ----------------Skills----------------

# 1
Skill.create!(name: "Git")

# 2
Skill.create!(name: "Ruby")

# 3
Skill.create!(name: "Test")

# 4
Skill.create!(name: "PHP")

# %
Skill.create!(name: "Java")

# 6
Skill.create!(name: "HTML/CSS")

# 7
Skill.create!(name: "Javascript")

# 8
Skill.create!(name: "C/C++")

# 9
Skill.create!(name: "C#")

# 10
Skill.create!(name: "Unity")

# 11
Skill.create!(name: "React-Native")

# 12
Skill.create!(name: "Android")

# 13
Skill.create!(name: "Swift")

# 14
Skill.create!(name: "Nodejs")

# 15
Skill.create!(name: "AI")

