class CreateTasksReports < ActiveRecord::Migration[5.2]
  def change
    create_table :tasks_reports do |t|
      t.integer :users_projects_task_id
      t.integer :report_id

      t.timestamps
    end
  end
end
