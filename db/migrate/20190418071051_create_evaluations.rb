class CreateEvaluations < ActiveRecord::Migration[5.2]
  def change
    create_table :evaluations do |t|
      t.integer :trainer_id
      t.integer :users_project_id
      t.string :title
      t.text :working_result
      t.text :working_attitude
      t.text :manners
      t.text :creation

      t.timestamps
    end
  end
end
