class AddDeletedToReports < ActiveRecord::Migration[5.2]
  def change
    add_column :reports, :deleted, :boolean, default: false
  end
end
