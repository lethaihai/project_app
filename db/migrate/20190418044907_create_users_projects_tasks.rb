class CreateUsersProjectsTasks < ActiveRecord::Migration[5.2]
  def change
    create_table :users_projects_tasks do |t|
      t.integer :users_project_id
      t.integer :task_id
      t.date :started
      t.date :finished
      t.boolean :status

      t.timestamps
    end
  end
end
