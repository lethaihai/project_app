class CreateReports < ActiveRecord::Migration[5.2]
  def change
    create_table :reports do |t|
      t.integer :trainee_id
      t.string :title
      t.text :content

      t.timestamps
    end
  end
end
