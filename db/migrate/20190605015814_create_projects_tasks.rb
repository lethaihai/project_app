class CreateProjectsTasks < ActiveRecord::Migration[5.2]
  def change
    create_table :projects_tasks do |t|
      t.integer :project_id
      t.integer :task_id
      t.integer :parent_task_id, default: 0

      t.timestamps
    end
  end
end
