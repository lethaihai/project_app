class CreateUsersSkills < ActiveRecord::Migration[5.2]
  def change
    create_table :users_skills do |t|
      t.integer :trainer_id
      t.integer :skill_id

      t.timestamps
    end
  end
end
