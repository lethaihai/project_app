class CreateProjects < ActiveRecord::Migration[5.2]
  def change
    create_table :projects do |t|
      t.integer :trainer_id
      t.string :name
      t.string :duration
      t.text :description

      t.timestamps
    end
  end
end
