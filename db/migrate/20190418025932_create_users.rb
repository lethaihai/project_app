class CreateUsers < ActiveRecord::Migration[5.2]
  def change
    create_table :users do |t|
      t.string :name
      t.string :email
      t.string :role
      t.date :date_of_birth
      t.integer :telephone
      t.string :division
      t.text :introduction

      t.timestamps
    end
  end
end
