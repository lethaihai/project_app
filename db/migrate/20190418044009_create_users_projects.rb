class CreateUsersProjects < ActiveRecord::Migration[5.2]
  def change
    create_table :users_projects do |t|
      t.integer :trainee_id
      t.integer :project_id
      t.date :started
      t.date :finished
      t.boolean :status

      t.timestamps
    end
  end
end
