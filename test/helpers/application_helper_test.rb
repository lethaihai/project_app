require 'test_helper'

class ApplicationHelperTest < ActionView::TestCase
  test "full title helper" do
    assert_equal full_title, 'Project App'
    assert_equal full_title("Home"), 'Home | Project App'
  end
end