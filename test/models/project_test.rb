require 'test_helper'

class ProjectTest < ActiveSupport::TestCase
  def setup
    @user = users(:trongquang)
    @project = @user.projects.build(name: "Shopping Cart App", duration: "1 tháng",
                                    description: "Đây là 1 project để
                                    trainning Ruby on Rails.")
  end

  test "should be valid" do
    assert @project.valid?
  end

  test "trainer id should be presence" do
    @project.trainer_id = nil
    assert_not @project.valid?
  end

  test "name should be present" do
    @project.name = nil
    assert_not @project.valid?
  end

  test "name should not be too longer 105 characters" do
    @project.name = "a" * 106
    assert_not @project.valid?
  end

  test "duration should be present" do
    @project.duration = nil
    assert_not @project.valid?
  end

  test "description should not be too longer 305 characters" do
    @project.description = "a" * 306
    assert_not @project.valid?
  end
end
