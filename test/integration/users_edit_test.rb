require 'test_helper'

class UsersEditTest < ActionDispatch::IntegrationTest
  def setup
    @user = users(:thaihai)
  end

  test "unsuccessful edit" do
    log_in_as(@user)
    get edit_user_path(@user)
    assert assert_template 'users/edit'
    patch user_path(@user), params: { user: { name:  "",
                                              email: "user@invalid",
                                              role: "",
                                              date_of_birth: "",
                                              telephone: "",
                                              division: "",
                                              introduction: "",
                                              password:              "sea",
                                              password_confirmation: "side" } }
    assert assert_template 'users/edit'
  end

  test "successful edit friendly forwarding" do
    get edit_user_path(@user)
    log_in_as(@user)
    assert_redirected_to edit_user_path(@user)
    name =  "Le Thai Hai"
    email = "lethaihai666@gmail.com"
    role = "admin"
    date_of_birth = Date.parse("15/01/1997")
    telephone = 392892686
    division = "D2"
    introduction = "Hello"
    patch user_path(@user), params: { user: { name:  name,
                                            email: email,
                                            role: role,
                                            date_of_birth: date_of_birth,
                                            telephone: telephone,
                                            division: division,
                                            introduction: introduction,
                                            password:              "",
                                            password_confirmation: "" } }
    assert_not flash.empty?
    assert_redirected_to @user
    @user.reload
    assert_equal name, @user.name
    assert_equal email, @user.email
    assert_equal role, @user.role
    # assert_equal date_of_birth, @user.date_of_birth
    assert_equal telephone, @user.telephone
    assert_equal division, @user.division
    assert_equal introduction, @user.introduction
    log_in_as(@user)
    assert_redirected_to root_url
  end
end
