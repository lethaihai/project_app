Rails.application.routes.draw do
  resources :places
  root 'static_pages#home'
  get '/signup', to: 'users#new'
  post '/signup', to: 'users#create'
  get '/login', to: 'sessions#new'
  post '/login', to: 'sessions#create'
  delete '/logout', to: 'sessions#destroy'
  resources :users do
    scope module: 'users' do
      resources :users_skills
    end
  end
  resources :account_activations, only: [:edit]
  resources :password_resets,     only: [:new, :create, :edit, :update]
  resources :projects do
    member do
      get :trainees
      # get :tasks
    end
    scope module: 'projects' do
      resources :evaluations
      resources :tasks do
        member do
          get :trainees
        end
        scope module: 'tasks' do
          resources :reports
        end
      end
    end
  end
  resources :users_projects
  resources :tasks
  resources :projects_tasks
  resources :users_projects_tasks
  resources :tasks_reports
  resources :skills
end
