FROM ruby:2.5.3

# Install apt based dependencies required to run Rails as
# well as RubyGems. As the Ruby image itself is based on a
# Debian image, we use apt-get to install those.
RUN apt-get update && apt-get install -y \
  build-essential \
  nodejs
RUN apt-get -y update
RUN apt-get -y upgrade
RUN apt-get update && apt-get install -y mysql-client postgresql-client sqlite3
RUN apt-get install -y imagemagick

# Configure the main working directory. This is the base
# directory used in any further RUN, COPY, and ENTRYPOINT
# commands.
RUN mkdir -p /app
WORKDIR /app

# Copy the Gemfile as well as the Gemfile.lock and install
# the RubyGems. This is a separate step so the dependencies
# will be cached unless changes to one of those two files
# are made.
COPY Gemfile Gemfile.lock ./
COPY . ./

RUN gem install bundler && bundle install --jobs 20 --retry 5

# Copy the main application.

# Expose port 3000 to the Docker host, so we can access it
# from the outside.
EXPOSE 4000

# The main command to run when the container starts. Also
# tell the Rails dev server to bind to all interfaces by
# default.
RUN bundle install
