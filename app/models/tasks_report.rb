class TasksReport < ApplicationRecord
  belongs_to :task, class_name: "UsersProjectsTask", foreign_key: "users_projects_task_id"
  belongs_to :report, class_name: "Report", foreign_key: "report_id"
  validates :users_projects_task_id, presence: true
  validates :report_id, uniqueness: { scope: :users_projects_task_id}
end
