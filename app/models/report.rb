class Report < ApplicationRecord
  belongs_to :trainee, class_name: 'User', foreign_key: 'trainee_id'
  has_many :belong_tasks, class_name: "TasksReport", foreign_key: "report_id", inverse_of: "report", dependent: :destroy
  has_many :belongs_tasks, through: :belong_tasks, source: :task
  accepts_nested_attributes_for :belong_tasks
  validates :trainee_id, presence: true
  validates :title, presence: true, length: { maximum: 105 }
  validates :content, length: { maximum: 305 }
end
