class Task < ApplicationRecord
  has_many :belong_projects, class_name: "ProjectsTask", foreign_key: "task_id", inverse_of: "task", dependent: :destroy
  has_many :belongs_projects, through: :belong_projects, source: :project
  has_many :belong_users_projects, class_name: "UsersProjectsTask", foreign_key: "task_id", inverse_of: "task", dependent: :destroy
  has_many :belongs_users_projects, through: :belong_users_projects, source: :users_project
  validates :name, presence: true, length: { maximum: 105 }, uniqueness: { case_sensitive: false }
  validates :duration, presence: true
  validates :description, length: { maximum: 305 }
end
