class UsersProjectsTask < ApplicationRecord
  belongs_to :task, class_name: "Task", foreign_key: "task_id"
  belongs_to :users_project, class_name: "UsersProject", foreign_key: "users_project_id"
  has_many :add_reports, class_name: "TasksReport", foreign_key: "users_projects_task_id", inverse_of: "task", dependent: :destroy
  has_many :added_reports, through: :add_reports, source: :report
  validates :task_id, presence: true
  validates :users_project_id, presence: true
  validates :task_id, uniqueness: { scope: :users_project_id}
end
