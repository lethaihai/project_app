class Skill < ApplicationRecord
  # has_many :belong_trainers, class_name: "UsersSkill", foreign_key: "skill_id", inverse_of: "skill", dependent: :destroy
  # has_many :belongs_trainers, through: :belong_trainers, source: :trainer
  has_and_belongs_to_many :users, dependent: :destroy
  validates :name, presence: true, length: { maximum: 105 }, uniqueness: { case_sensitive: false }
end
