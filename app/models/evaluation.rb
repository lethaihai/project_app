class Evaluation < ApplicationRecord
  belongs_to :trainer, class_name: "User", foreign_key: "trainer_id"
  belongs_to :user_project, class_name: "UsersProject", foreign_key: "users_project_id"
  validates :trainer_id, presence: true
  validates :users_project_id, presence: true
  validates :users_project_id, uniqueness: { scope: :trainer_id }
  validates :title, presence: true, length: { maximum: 105 }, uniqueness: { case_sensitive: false }
  validates :working_result, length: { maximum: 305 }
  validates :working_attitude, length: { maximum: 305 }
  validates :manners, length: { maximum: 305 }
  validates :creation, length: { maximum: 305 }
end
