class User < ApplicationRecord
  has_many :projects, foreign_key: 'trainer_id', inverse_of: "trainer", dependent: :destroy
  has_many :join_projects, class_name: "UsersProject", foreign_key: 'trainee_id', inverse_of: "trainee", dependent: :destroy
  has_many :joined_projects, through: :join_projects, source: :project
  has_many :reports, class_name: "Report", foreign_key: "trainee_id", inverse_of: "trainee", dependent: :destroy
  has_many :evaluations, class_name: "Evaluation", foreign_key: "trainer_id", inverse_of: "trainer", dependent: :destroy
  # has_many :add_skills, class_name: "UsersSkill", foreign_key: "trainer_id", inverse_of: "trainer", dependent: :destroy
  # has_many :added_skills, through: :add_skills, source: :skill
  has_and_belongs_to_many :skills
  attr_accessor :remember_token, :activation_token, :reset_token
  before_save :downcase_email
  before_create :create_activation_digest
  mount_uploader :picture, PictureUploader
  validates :name, presence: true, length: { maximum: 50 }
  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-]+(\.[a-z\d\-]+)*\.[a-z]+\z/i
  validates :email, presence: true, length: { maximum: 255 },
                    format: { with: VALID_EMAIL_REGEX },
                    uniqueness: { case_sensitive: false}
  validates :role, presence: true, length: { maximum: 7 }
  # validates :date_of_birth
  validates :telephone, length: { maximum: 11}
  # validates :division
  validates :introduction, length: { maximum: 500}
  # VALID_DATE_REGEX = /^(?:0[1-9]|[12]\d|3[01])([\/.-])(?:0[1-9]|1[12])\1(?:19|20)\d\d$/
  # validates :date_of_birth, format: { with: VALID_DATE_REGEX }
  has_secure_password
  validates :password, presence: true, length: { minimum: 8 }, allow_nil: true
  validate :picture_size

  # Returns the hash digest of the given string.
  def User.digest(string)
    cost = ActiveModel::SecurePassword.min_cost ? BCrypt::Engine::MIN_COST :
               BCrypt::Engine.cost
    BCrypt::Password.create(string, cost: cost)
  end

  # Returns a random token.
  def User.new_token
    SecureRandom.urlsafe_base64
  end

  # Remembers a user in the database for use in persistent sessions.
  def remember
    self.remember_token = User.new_token
    update_attribute(:remember_digest, User.digest(remember_token))
  end

  # Returns true if the given token matches the digest.
  def authenticated?(attribute, token)
    digest = send("#{attribute}_digest")
    return false if digest.nil?
    BCrypt::Password.new(digest).is_password?(token)
  end

  # Forget a user.
  def forget
    update_attribute(:remember_digest, nil)
  end

  # Activates an account.
  def activate
    update_columns(activated: true, activated_at: Time.zone.now)
  end

  # Sends activation email.
  def send_activation_email
    UserMailer.account_activation(self).deliver_now
  end

  # Sets the password reset attributes.
  def create_reset_digest
    self.reset_token = User.new_token
    update_columns(reset_digest: User.digest(reset_token), reset_sent_at: Time.zone.now)
  end

  # Sends password reset email.
  def send_password_reset_email
    UserMailer.password_reset(self).deliver_now
  end

  # Returns true if a password reset has expired.
  def password_reset_expired?
    reset_sent_at < 2.hours.ago
  end

  # Join a project
  def join_project(project)
    joined_projects << project
  end

  # Leave a project
  def leave_project(project)
    joined_projects.delete(project)
  end

  # Return true if this user joined a project
  def join_project?(project)
    joined_projects.include?(project)
  end

  private

    # Converts email to all lower-case.
    def downcase_email
      email.downcase!
    end

    # Creates and assigns the activation token and digest.
    def create_activation_digest
      self.activation_token  = User.new_token
      self.activation_digest = User.digest(activation_token)
    end

    # Validates the size of an uploaded picture.
    def picture_size
      if picture.size > 5.megabytes
        errors.add(:picture, "should be less than 5MB")
      end
    end
end
