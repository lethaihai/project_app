class UsersProject < ApplicationRecord
  belongs_to :trainee, class_name: "User", foreign_key: "trainee_id"
  belongs_to :project, class_name: "Project", foreign_key: "project_id"
  has_many :add_tasks, class_name: 'UsersProjectsTask', foreign_key: 'users_project_id', inverse_of: "users_project", dependent: :destroy
  has_many :added_tasks, through: :add_tasks, source: :task
  has_one :evaluation, class_name: "Evaluation", foreign_key: "users_project_id", inverse_of: "user_project", dependent: :destroy
  validates :trainee_id, presence: true
  validates :project_id, presence: true
  validates :trainee_id, uniqueness: { scope: :project_id}
end
