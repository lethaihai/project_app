class Project < ApplicationRecord
  belongs_to :trainer, class_name: 'User', foreign_key: 'trainer_id'
  has_many :add_trainees, class_name: 'UsersProject', foreign_key: 'project_id', inverse_of: "project", dependent: :destroy
  has_many :added_trainees, through: :add_trainees, source: :trainee
  has_many :add_tasks, class_name: 'ProjectsTask', foreign_key: 'project_id', inverse_of: "project", dependent: :destroy
  has_many :added_tasks, through: :add_tasks, source: :task
  validates :trainer_id, presence: true
  validates :name, presence: true, length: { maximum: 105 }
  validates :duration, presence: true
  validates :description, length: { maximum: 305 }

  # Add a trainee.
  def add_trainee(trainee)
    added_trainees << trainee
  end

  # Remove a trainee.
  def remove_trainee(trainee)
    added_trainees.delete(trainee)
  end

  # Return true if this project added a trainee.
  def add_trainee?(trainee)
    added_trainees.include?(trainee)
  end
end
