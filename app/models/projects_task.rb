class ProjectsTask < ApplicationRecord
  belongs_to :task, class_name: "Task", foreign_key: "task_id"
  belongs_to :project, class_name: "Project", foreign_key: "project_id"
  has_many :child, class_name: "ProjectsTask", foreign_key: "parent_task_id", inverse_of: "parent", dependent: :destroy
  belongs_to :parent, class_name: "ProjectsTask", foreign_key: "parent_task_id", optional: true
  validates :task_id, presence: true
  validates :project_id, presence: true
  validates :parent_task_id, presence: true
  validates :task_id, uniqueness: { scope: :project_id}
  validate :parent_not_itself

  private

  def parent_not_itself
    if parent_task_id == id
        errors.add(:parent_task_id, "can't be itself")
    end
  end
end
