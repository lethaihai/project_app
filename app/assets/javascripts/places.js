function createInfoWindow(text){
    var infowindow = new google.maps.InfoWindow({
        content: text
    });
    return infowindow;
}

function initMap(lat, lng) {
    var myCoords = new google.maps.LatLng(lat, lng);
    var mapOptions = {
        center: myCoords,
        zoom: 17,
        panControl:true,
        zoomControl:true,
        mapTypeControl:true,
        scaleControl:true,
        streetViewControl:true,
        overviewMapControl:true,
        rotateControl:true
    };
    var map = new google.maps.Map(document.getElementById('map'), mapOptions);
    var marker = new google.maps.Marker({
        position: myCoords,
        map: map
    });
    var info = createInfoWindow("Công ty TNHH Rikkeisoft Hà Nội </br>" +
        "Tầng 3, Tòa nhà SUDICO (HH3) </br>" +
        "Đường Mễ Trì </br>" +
        "Phường Mỹ Đình 1 </br>" +
        "Quận Nam Từ Liêm </br>" +
        "Thành phố Hà Nội, Việt Nam </br>" +
        "<a target='blank' href='https://www.google.com/maps/place/Rikkeisoft/@21.0165022,105.7785332,18z/data=!3m1!4b1!4m5!3m4!1s0x313454ac8f4b0465:0xfd5cbbbead5513c0!8m2!3d21.0165001!4d105.7794534'>Xem trên Google Maps</a>");
    google.maps.event.addListener(marker, 'click', function() {
        info.open(map,marker);
    });
}

function initMap2() {
    var lat = document.getElementById('place_latitude').value;
    var lng = document.getElementById('place_longitude').value;

    // if not defined create default position
    if (!lat || !lng){
        lat=21.016492;
        lng=105.779465;
        document.getElementById('place_latitude').value = lat;
        document.getElementById('place_longitude').value = lng;
    }
    var myCoords = new google.maps.LatLng(lat, lng);
    var mapOptions = {
        center: myCoords,
        zoom: 17
    };
    var map = new google.maps.Map(document.getElementById('map2'), mapOptions);
    var marker = new google.maps.Marker({
        position: myCoords,
        animation: google.maps.Animation.DROP,
        map: map,
        draggable: true
    });
    // refresh marker position and recenter map on marker
    function refreshMarker(){
        var lat = document.getElementById('place_latitude').value;
        var lng = document.getElementById('place_longitude').value;
        var myCoords = new google.maps.LatLng(lat, lng);
        marker.setPosition(myCoords);
        map.setCenter(marker.getPosition());
    }
    // when input values change call refreshMarker
    document.getElementById('place_latitude').onchange = refreshMarker;
    document.getElementById('place_longitude').onchange = refreshMarker;
    // when marker is dragged update input values
    marker.addListener('drag', function() {
        latlng = marker.getPosition();
        newlat=(Math.round(latlng.lat()*1000000))/1000000;
        newlng=(Math.round(latlng.lng()*1000000))/1000000;
        document.getElementById('place_latitude').value = newlat;
        document.getElementById('place_longitude').value = newlng;
    });
    // When drag ends, center (pan) the map on the marker position
    marker.addListener('dragend', function() {
        map.panTo(marker.getPosition());
    });
    var info = createInfoWindow("Công ty TNHH Rikkeisoft Hà Nội </br>" +
        "Tầng 3, Tòa nhà SUDICO (HH3) </br>" +
        "Đường Mễ Trì </br>" +
        "Phường Mỹ Đình 1 </br>" +
        "Quận Nam Từ Liêm </br>" +
        "Thành phố Hà Nội, Việt Nam </br>" +
        "<a target='blank' href='https://www.google.com/maps/place/Rikkeisoft/@21.0165022,105.7785332,18z/data=!3m1!4b1!4m5!3m4!1s0x313454ac8f4b0465:0xfd5cbbbead5513c0!8m2!3d21.0165001!4d105.7794534'>Xem trên Google Maps</a>");
    google.maps.event.addListener(marker, 'click', function() {
        info.open(map,marker);
    });
}