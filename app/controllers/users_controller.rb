class UsersController < ApplicationController
  before_action :logged_in_user, only: [:edit, :update, :index, :destroy, :show] # write in application_controller.rb
  before_action :correct_user, only: [:edit, :update]
  before_action :admin_user, only: :destroy

  def show
    @user = User.find(params[:id])
    redirect_to root_url and return unless @user.activated?
  end

  def index
    if admin?
      @search = User.where(activated: true, deleted: false).ransack(params[:q])
    elsif trainer?
      @search = User.where(role: "trainee", activated: true, deleted: false).ransack(params[:q])
    elsif trainee?
      @search = User.where(role: "trainer", activated: true, deleted: false).ransack(params[:q])
    end
    @users_search = @search.result(distinct: true).paginate(:per_page => 10, page: params[:page])
  end

  def new
    @user = User.new
  end

  def create
    @user = User.new(user_params)
    if @user.save
      @user.send_activation_email
      flash[:info] = "Please check your email to activate your account!"
      redirect_to root_url
    else
      render 'new'
    end
  end

  def edit
    @user = User.find(params[:id])
  end

  def update
    @user = User.find(params[:id])
    if @user.update_attributes(user_params)
      flash[:success] = "User #{@user.name} has been updated"
      redirect_to @user
    else
      render 'edit'
    end
  end

  def destroy
    @user = User.find(params[:id])
    @user.update_attributes(deleted: true)
    @user.projects.update_all(deleted: true)
    flash[:success] = "User #{@user.name} has been deleted"
    redirect_to users_url
  end

  private

  def user_params
    params.require(:user).permit(:name, :email, :role, :date_of_birth, :telephone,
                                 :division, :introduction, :password,
                                 :password_confirmation, :picture, skill_ids:[])
  end

  # Confirms a correct user.
  def correct_user
    if !admin?
      @user = User.find(params[:id])
      redirect_to(root_url) unless current_user?(@user)
    end
  end

  # Confirms an admin user.
  def admin_user
    redirect_to(root_url) unless admin?
  end
end
