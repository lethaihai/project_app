class UsersProjectsController < ApplicationController
  before_action :logged_in_user, only: [:show, :new, :create, :edit, :update, :destroy] # write in application_controller.rb
  before_action :dont_have_any_project, only: [:new, :create, :edit, :update, :destroy]
  before_action :redirect_to_trainees_project, only: :show
  # before_action :redirect_to_projects, only: :index
  before_action :admin_or_trainer, only: [:new, :edit, :create, :destroy]
  before_action :find_project, only: [:new, :create]

  def index
  end

  def show
  end

  def new
    @projectc = Project.find_by id: params[:project_id]
    if @projectc.added_tasks.empty?
      flash[:info] = "Please create, add and setup tasks for project before add trainee"
      redirect_to project_tasks_path(@projectc)
    else
      if admin?
        @users_project = UsersProject.new
        @projects_add = Project.where(deleted: false)
      else trainer?
      @users_project = UsersProject.new
      @projects_add = current_user.projects.where(deleted: false)
      end
      @trainees_add = User.where(role: 'trainee', deleted: false)
      @trainees_can_add = []
      @trainees_add.each do |t|
        if t.joined_projects.empty?
          @trainees_can_add << t
        end
      end
    end
  end

  def create
    if admin?
      @users_project = UsersProject.new(users_project_params)
      @projects_add = Project.where(deleted: false)
    else trainer?
      @users_project = UsersProject.new(users_project_params)
      @projects_add = current_user.projects.where(deleted: false)
    end
    @trainees_add = User.where(role: 'trainee', deleted: false)
    if @users_project.trainee.joined_projects.any?
      flash[:warning] = "This trainee has joined any project"
      render 'new'
    elsif @users_project.save
      flash[:success] = "Add trainee #{@users_project.trainee.name} to
                         project #{@users_project.project.name} successful"
      redirect_to trainees_project_path(@users_project.project)
    else
      render 'new'
    end
  end

  def edit
    @users_project = UsersProject.find(params[:id])
  end

  def update
    @users_project = UsersProject.find(params[:id])
    if @users_project.update_attributes(users_project_params)
      if @users_project.started.nil?
        @users_project.update_attributes(finished: nil, status: nil)
        flash[:success] = "Trainee #{@users_project.trainee.name} not started
                           project #{@users_project.project.name} yet"
        redirect_to @users_project.project
      elsif !@users_project.started.nil? && @users_project.finished.nil?
        @users_project.update_attributes(status: false)
        flash[:success] = "Trainee #{@users_project.trainee.name} has started
                           project #{@users_project.project.name}"
        redirect_to @users_project.project
      elsif !@users_project.started.nil? && !@users_project.finished.nil?
        @users_project.update_attributes(status: true)
        flash[:success] = "Trainee #{@users_project.trainee.name} has finished
                           project #{@users_project.project.name}"
        redirect_to @users_project.project
      end
    else
      render 'edit'
    end
  end

  def destroy
    @users_project = UsersProject.find(params[:id])
    @users_project.destroy
    flash[:success] = "Trainee #{@users_project.trainee.name} has been removed
                       from project #{@users_project.project.name}"
    redirect_to trainees_project_path(@users_project.project)
  end

  private

  # Allow change attributes on Web
  def users_project_params
    if admin? || trainer?
      params.require(:users_project).permit(:project_id, :trainee_id, :started, :finished, :status)
    else trainee?
      params.permit(:project_id, :trainee_id, :started, :finished, :status)
    end
  end

  def dont_have_any_project
    if current_user.projects.empty? && !admin? && current_user.joined_projects.empty?
      flash[:info] = "You don't have or belong to any project"
      redirect_to projects_url
    end
  end

  # Confirm an admin or trainer
  def admin_or_trainer
    unless admin? || trainer?
      flash[:warning] = "Just admin or trainer can access that action with project"
      redirect_to(projects_url)
    end
  end

  # Redirect when show
  def redirect_to_trainees_project
    @users_project = UsersProject.find(params[:id])
    redirect_to trainees_project_path(@users_project.project)
  end

  # Redirect when index
  def redirect_to_projects
    redirect_to projects_url
  end

  def find_project
    @project = Project.find_by id: params[:project_id]
  end
end
