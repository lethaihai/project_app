class TasksController < ApplicationController
  before_action :logged_in_user, only: [:show, :new, :create, :edit, :update, :index, :destroy] # write in application_controller.rb
  before_action :dont_have_any_project, only: [:show, :new, :create, :edit, :update, :index, :destroy]
  before_action :correct_user_project_task, only: :show
  before_action :admin_or_trainer, only: [:new, :create]
  before_action :admin_user, only: [:edit, :update, :index, :destroy]

  def show
    @task = Task.find(params[:id])
  end

  def new
    @task = Task.new
  end

  def create
    @task = Task.new(task_params)
    if @task.save
      flash[:success] = "Create task successful"
      redirect_to @task
    else
      render 'new'
    end
  end

  def edit
    @task = Task.find(params[:id])
  end

  def update
    @task = Task.find(params[:id])
    if @task.update_attributes(task_params)
      flash[:success] = "Task #{@task.name} has been updated"
      redirect_to @task
    else
      render 'edit'
    end
  end

  def index
    @search = Task.where(deleted: false).ransack(params[:q])
    @tasks = @search.result(distinct: true).paginate(:per_page => 10, page: params[:page])
  end

  def destroy
    @task = Task.find(params[:id])
    @task.update_attribute(:deleted, true)
    flash[:success] = "Task #{@task.name} has been deleted"
    redirect_to tasks_url
  end

  private

  # Allow change attributes on Web
  def task_params
    if admin? || trainer?
      params.require(:task).permit(:name, :duration, :description)
    end
  end

  def dont_have_any_project
    if current_user.projects.empty? && !admin? && current_user.joined_projects.empty?
      flash[:info] = "You don't have or belong to any project"
      redirect_to projects_url
    end
  end

  # Confirm an admin or trainer
  def admin_or_trainer
    unless admin? || trainer?
      flash[:warning] = "Just admin or trainer can access that action with project"
      redirect_to(projects_url)
    end
  end

  # Confirm an admin user
  def admin_user
    unless admin?
      flash[:warning] = "Just admin can access that action with task"
      redirect_to(projects_url)
    end
  end

  # Confirm correct user project task
  def correct_user_project_task
    if admin?
      @task = Task.find(params[:id])
    elsif trainee?
      @task = Task.find(params[:id])
      @projects = current_user.joined_projects
      @task_can_view = []
      @projects.each do |p|
        p.added_tasks.each do |t|
          @task_can_view << t.id
        end
      end
      if @task_can_view.exclude?(@task.id)
        flash[:warning] = "You are not allow to access #{@task.name} task"
        redirect_to projects_url
      end
    else trainer?
      @task = Task.find(params[:id])
      @projects = current_user.projects
      @task_can_view = []
      @projects.each do |p|
        p.added_tasks.each do |t|
          @task_can_view << t.id
        end
      end
      if @task_can_view.exclude?(@task.id)
        flash[:warning] = "You are not allow to access #{@task.name} task"
        redirect_to projects_url
      end
    end
  end
end
