class ProjectsController < ApplicationController
  before_action :logged_in_user, only: [:show, :new, :create, :edit, :update, :index, :destroy, :trainees] # write in application_controller.rb
  before_action :dont_belong_any_project, only: [:show, :new, :create, :edit, :update, :index, :destroy, :trainees]
  before_action :dont_have_any_project, only: [:show, :edit, :update, :destroy, :trainees]
  before_action :correct_user_project, only: [:show, :edit, :update, :destroy, :trainees]
  before_action :admin_or_trainer, only: [:new, :create, :edit, :update, :destroy]

  def show
    if admin?
      @project = Project.find(params[:id])
    elsif trainer?
      @project = current_user.projects.find(params[:id])
    elsif trainee?
      @project = current_user.joined_projects.find(params[:id])
      @user_project = current_user.join_projects.find_by(project_id: @project.id)
      @users_project_tasks = UsersProjectsTask.where(users_project_id: @user_project.id)
      @count_tasks_finished = 0
      @users_project_tasks.each do |task|
        if task.finished.present?
          @count_tasks_finished += 1
        end
      end
    end
  end

  def index
    if admin?
      @search = Project.where(deleted: false).ransack(params[:q])
    elsif trainee?
      @search = current_user.joined_projects.where(deleted: false).ransack(params[:q])
    elsif trainer?
      @search = current_user.projects.where(deleted: false).ransack(params[:q])
    end
    @projects = @search.result(distinct: true).paginate(:per_page => 10, page: params[:page])
  end

  def new
    if admin?
      @project = Project.new
    else
      @project = current_user.projects.build
    end
    @trainers = User.where(role: "trainer", deleted: false)
  end

  def create
    if admin?
      @project = Project.new(project_params)
    else
      @project = current_user.projects.build(project_params)
    end
    @trainers = User.where(role: "trainer", deleted: false)
    if @project.save
      flash[:success] = "Create project successful"
      redirect_to @project
    else
      render 'new'
    end
  end

  def edit
    if admin?
      @project = Project.find(params[:id])
    else
      @project = current_user.projects.find(params[:id])
    end
    @trainers = User.where(role: "trainer", deleted: false)
  end

  def update
    if admin?
      @project = Project.find(params[:id])
    else
      @project = current_user.projects.find(params[:id])
    end
    @trainers = User.where(role: "trainer", deleted: false)
    if @project.update_attributes(project_params)
      flash[:success] = "Project #{@project.name} has been updated"
      redirect_to @project
    else
      render 'edit'
    end
  end

  def destroy
    if admin?
      @project = Project.find(params[:id])
      @project.update_attribute(:deleted, true)
      flash[:success] = "Project #{@project.name} has been deleted"
      redirect_to projects_url
    elsif trainer?
      @project = current_user.projects.find(params[:id])
      @project.update_attribute(:deleted, true)
      flash[:success] = "Project #{@project.name} has been deleted"
      redirect_to projects_url
    end
  end

  def trainees
    @project = Project.find(params[:id])
    @project_trainees = @project.added_trainees.where(deleted: false).order(status: :asc).paginate(:per_page => 5, page: params[:page])
    @user_projects = UsersProject.where(project_id: @project.id)
  end

  private

  # Allow change attributes on Web
  def project_params
    if admin?
      params.require(:project).permit(:name, :trainer_id, :duration, :description)
    else
      params.require(:project).permit(:name, :duration, :description)
    end
  end

  def dont_have_any_project
    if current_user.projects.empty? && trainer?
      flash[:info] = "You don't have any project, create one!"
      redirect_to projects_url
    end
  end

  def dont_belong_any_project
    if current_user.joined_projects.empty? && trainee?
      flash[:info] = "You don't belong any projects. Contact to your trainer"
      redirect_to users_path
    end
  end

  def correct_user_project
    @project = Project.find(params[:id])
    if !current_user.projects.ids.include?(@project.id) && !admin? && !current_user.joined_projects.ids.include?(@project.id)
      flash[:warning] = "You are not allowed to access project #{@project.name}"
      redirect_to projects_url
    end
  end

  # Confirm an admin or trainer
  def admin_or_trainer
    unless admin? || trainer?
      flash[:warning] = "Just admin or trainer can access that action with project"
      redirect_to projects_url
    end
  end
end
