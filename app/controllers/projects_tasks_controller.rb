class ProjectsTasksController < ApplicationController
  before_action :logged_in_user, only: [:show, :new, :create, :edit, :update, :index, :destroy] # write in application_controller.rb
  before_action :dont_have_any_project, only: [:new, :create, :edit, :update, :destroy]
  before_action :redirect_to_trainees_project, only: :show
  before_action :redirect_to_projects, only: :index
  before_action :admin_or_trainer, only: [:new, :edit, :update, :create, :destroy]
  before_action :find_project, only: [:new, :create]

  def index
  end

  def show
  end

  def new
    if admin?
      @projects_task = ProjectsTask.new
      @projects_add = Project.where(deleted: false)
    else trainer?
      @projects_task = ProjectsTask.new
      @projects_add = current_user.projects.where(deleted: false)
    end
    @tasks_add = Task.where(deleted: false)
  end

  def create
    if admin?
      @projects_task = ProjectsTask.new(projects_task_params)
      @projects_add = Project.where(deleted: false)
    else trainer?
      @projects_task = ProjectsTask.new(projects_task_params)
      @projects_add = current_user.projects.where(deleted: false)
    end
    @tasks_add = Task.where(deleted: false)
    @parent = ProjectsTask.where(project_id: @projects_task.project_id)
    if @projects_task.save
      flash[:success] = "Add task #{@projects_task.task.name} to
                         project #{@projects_task.project.name} successful"
      redirect_to project_tasks_path(@projects_task.project)
    else
      render 'new'
    end
  end

  def edit
    @projects_task = ProjectsTask.find(params[:id])
    @parent = ProjectsTask.where(project_id: @projects_task.project_id)
  end

  def update
    @projects_task = ProjectsTask.find(params[:id])
    @parent = ProjectsTask.where(project_id: @projects_task.project_id)
    if @projects_task.update_attributes(projects_task_params)
      flash[:success] = "Parent of task #{@projects_task.task.name}
                         in project #{@projects_task.project.name} has been updated"
      redirect_to project_tasks_path(@projects_task.project)
    else
      render 'edit'
    end
  end

  def destroy
    @projects_task = ProjectsTask.find(params[:id])
    if ProjectsTask.all.map(&:parent_task_id).any?{|p| p == @projects_task.id }
      ProjectsTask.where(parent_task_id: @projects_task.id).update_all(parent_task_id: 0)
    end
    @projects_task.destroy
    flash[:success] = "Task #{@projects_task.task.name} has been removed
                       from project #{@projects_task.project.name}"
    redirect_to project_tasks_path(@projects_task.project)
  end

  private

  # Allow change attributes on Web
  def projects_task_params
    if admin? || trainer?
      params.require(:projects_task).permit(:project_id, :task_id, :parent_task_id)
    end
  end

  def dont_have_any_project
    if current_user.projects.empty? && !admin? && current_user.joined_projects.empty?
      flash[:info] = "You don't have or belong to any project"
      redirect_to projects_url
    end
  end

  # Confirm an admin or trainer
  def admin_or_trainer
    unless admin? || trainer?
      flash[:warning] = "Just admin or trainer can access that action with project"
      redirect_to projects_url
    end
  end

  # Redirect when show
  def redirect_to_trainees_project
    @projects_task = ProjectsTask.find(params[:id])
    redirect_to project_tasks_path(@projects_task.project)
  end

  # Redirect when index
  def redirect_to_projects
    redirect_to projects_url
  end

  def find_project
    @project = Project.find_by id: params[:project_id]
  end
end
