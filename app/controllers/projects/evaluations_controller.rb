class Projects::EvaluationsController < ApplicationController
  before_action :logged_in_user, only: [:show, :new, :create, :edit, :update, :index, :destroy] # write in application_controller.rb
  before_action :dont_have_any_project, only: [:show, :new, :create, :edit, :update, :destroy]
  before_action :correct_user_project, only: [:show, :new, :create, :edit, :update, :destroy]
  before_action :correct_project_evaluation, only: [:show, :edit, :update, :destroy]
  before_action :correct_user_evaluation, only: :show
  before_action :admin_or_trainer, only: [:new, :create, :edit, :update, :destroy]

  def show
    @project = Project.find(params[:project_id])
    if admin?
      @evaluation = Evaluation.find(params[:id])
    elsif trainer?
      @evaluation = current_user.evaluations.find(params[:id])
    elsif trainee?
      @evaluation = current_user.join_projects.find_by(project_id: @project.id).evaluation
    end
  end

  def new
    @project = Project.find(params[:project_id])
    @users_projects = @project.add_trainees.where(status: true)
    if admin?
      @evaluation = Evaluation.new
    elsif trainer?
      @evaluation = current_user.evaluations.build
    end
  end

  def create
    @project = Project.find(params[:project_id])
    @users_projects = @project.add_trainees.where(status: true)
    if admin?
      @evaluation = Evaluation.new(evaluation_params)
    elsif trainer?
      @evaluation = current_user.evaluations.build(evaluation_params)
    end
    if @evaluation.save
      flash[:success] = "Create evaluation successful"
      redirect_to project_evaluation_path(@project.id, @evaluation)
    else
      render 'new'
    end
  end

  def edit
    @project = Project.find(params[:project_id])
    if admin?
      @evaluation = Evaluation.find(params[:id])
    elsif trainer?
      @evaluation = current_user.evaluations.find(params[:id])
    end
  end

  def update
    @project = Project.find(params[:project_id])
    if admin?
      @evaluation = Evaluation.find(params[:id])
    elsif trainer?
      @evaluation = current_user.evaluations.find(params[:id])
    end
    if @evaluation.update_attributes(evaluation_params)
      flash[:success] = "Evaluation #{@evaluation.title} has been updated"
      redirect_to project_evaluation_path(@project.id, @evaluation)
    else
      render 'edit'
    end
  end

  def destroy
    @project = Project.find(params[:project_id])
    if admin?
      @evaluation = Evaluation.find(params[:id])
    elsif trainer?
      @evaluation = current_user.evaluations.find(params[:id])
    end
    @evaluation.destroy
    flash[:success] = "Evaluation #{@evaluation.title} has been deleted"
    redirect_to trainees_project_path(@project)
  end

  # Allow change attributes on Web
  def evaluation_params
    if admin? || trainer?
      params.require(:evaluation).permit(:title, :trainer_id, :users_project_id, :working_result, :working_attitude, :manners, :creation)
    end
  end

  def dont_have_any_project
    if current_user.projects.empty? && !admin? && current_user.joined_projects.empty?
      flash[:info] = "You don't have or belong to any project"
      redirect_to projects_url
    end
  end

  def correct_user_project
    @project = Project.find(params[:project_id])
    if !current_user.projects.ids.include?(@project.id) && !admin? && !current_user.joined_projects.ids.include?(@project.id)
      flash[:warning] = "You are not allowed to access project #{@project.name}"
      redirect_to projects_url
    end
  end

  # Confirm an admin or trainer
  def admin_or_trainer
    unless admin? || trainer?
      flash[:warning] = "Just admin or trainer can access that action with evaluation"
      redirect_to projects_url
    end
  end

  # Confirm correct project evaluation when project/id/evaluation/id
  def correct_project_evaluation
    @project = Project.find(params[:project_id])
    @evaluation_w = Evaluation.find(params[:id])
    if @evaluation_w.user_project.project.id != @project.id
      flash[:warning] = "Evaluation doesn't exist or not belongs to project #{@project.name}"
      redirect_to project_url(@project)
    end
  end

  # Confirm correct user can view evaluation when project/id/evaluation/id
  def correct_user_evaluation
    if trainee?
      @evaluation_w = Evaluation.find(params[:id])
      @evaluation = current_user.join_projects.find_by(project_id: @project.id).evaluation
      if @evaluation.present? && @evaluation_w.id != @evaluation.id
        flash[:warning] = "Evaluation not belongs you"
        redirect_to trainees_project_url(@project)
      elsif @evaluation.nil?
        flash[:warning] = "Evaluation doesn't exist or not belongs project #{@project.name}"
        redirect_to project_url(@project)
      end
    end
  end
end