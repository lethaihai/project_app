module Projects
  class Tasks::ReportsController < ApplicationController
    before_action :logged_in_user, only: [:show, :new, :create, :edit, :update, :index, :destroy] # write in application_controller.rb
    before_action :dont_have_any_project, only: [:show, :new, :create, :edit, :update, :index, :destroy]
    before_action :correct_user_project, only: [:show, :new, :create, :edit, :update, :destroy, :index]
    before_action :project_include_task, only: [:show, :new, :create, :edit, :update, :destroy, :index]
    before_action :correct_user_project_task, only: [:show, :new, :create, :edit, :update, :destroy, :index]
    before_action :correct_project_report, only: [:show, :edit, :update, :destroy]
    before_action :not_started_task_yet, only: [:show, :new, :create, :edit, :update, :destroy, :index]
    before_action :correct_task_report, only: [:show, :edit, :update, :destroy]
    before_action :trainee_user, only: [:new, :create, :edit, :update, :destroy]

    def show
      @project = Project.find(params[:project_id])
      @task = @project.added_tasks.find(params[:task_id])
      if admin? || trainer?
        @report = Report.find(params[:id])
      elsif trainee?
        @users_project = current_user.join_projects.find_by(project_id: @project.id)
        @users_projects_task = UsersProjectsTask.find_by(users_project_id: @users_project.id, task_id: @task.id)
        @reports = @users_projects_task.added_reports
        @report = @reports.find(params[:id])
      end
    end

    def new
      @project = Project.find(params[:project_id])
      @task = @project.added_tasks.find(params[:task_id])
      @users_project = current_user.join_projects.find_by(project_id: @project.id)
      @users_projects_task = UsersProjectsTask.find_by(users_project_id: @users_project.id, task_id: @task.id)
      @report = Report.new
      @report.belong_tasks.build
    end

    def create
      @project = Project.find(params[:project_id])
      @task = @project.added_tasks.find(params[:task_id])
      @users_project = current_user.join_projects.find_by(project_id: @project.id)
      @users_projects_task = UsersProjectsTask.find_by(users_project_id: @users_project.id, task_id: @task.id)
      @report = Report.new(report_params)
      if @report.save
        flash[:success] = "Create Report Successful"
        redirect_to project_task_report_path(@project.id, @task.id, @report)
      else
        render 'new'
      end
    end

    def edit
      @project = Project.find(params[:project_id])
      @task = @project.added_tasks.find(params[:task_id])
      # if admin?
      #   @report = Report.find(params[:id])
      # els
        if trainee?
        @users_project = current_user.join_projects.find_by(project_id: @project.id)
        @users_projects_task = UsersProjectsTask.find_by(users_project_id: @users_project.id, task_id: @task.id)
        @reports = @users_projects_task.added_reports
        @report = @reports.find(params[:id])
      end
    end

    def update
      @project = Project.find(params[:project_id])
      @task = @project.added_tasks.find(params[:task_id])
      if admin?
        @report = Report.find(params[:id])
      elsif trainee?
        @report = current_user.reports.find(params[:id])
      end
      if @report.update_attributes(report_params)
        flash[:success] = "Report #{@report.title} has been updated"
        redirect_to project_task_report_path(@project.id, @task.id, @report)
      else
        render 'edit'
      end
    end

    def index
      if trainee?
        @project = Project.find(params[:project_id])
        @task = @project.added_tasks.find(params[:task_id])
        @users_project = current_user.join_projects.find_by(project_id: @project.id)
        @users_projects_task = UsersProjectsTask.find_by(users_project_id: @users_project.id, task_id: @task.id)
        @reports = @users_projects_task.added_reports.where(deleted: false).paginate(:per_page => 15, page: params[:page])
      elsif admin? || trainer?
        @project = Project.find(params[:project_id])
        @task = @project.added_tasks.find(params[:task_id])
        @users_projects = @task.belongs_users_projects.where(project_id: @project.id).order(status: :asc).paginate(:per_page => 5, page: params[:page])
      end
    end

    def destroy
      @project = Project.find(params[:project_id])
      @task = @project.added_tasks.find(params[:task_id])
      # if admin? || trainer?
      #   @report = Report.find(params[:id])
      # els
      if trainee?
        @report = current_user.reports.find(params[:id])
        @users_project = current_user.join_projects.find_by(project_id: @project.id)
        @users_projects_task = UsersProjectsTask.find_by(users_project_id: @users_project.id, task_id: @task.id)
      end
      @report.update_attributes(deleted: true)
      @report.belong_tasks.find_by(users_projects_task_id: @users_projects_task.id).destroy
      flash[:success] = "Report #{@report.title} has been deleted"
      redirect_to project_task_reports_path(@project.id, @task.id)
    end

    private

    def report_params
      params.require(:report).permit(:trainee_id, :title, :content, belong_tasks_attributes: [:users_projects_task_id])
    end

    def dont_have_any_project
      if current_user.projects.empty? && !admin? && current_user.joined_projects.empty?
        flash[:info] = "You don't have or belong to any project"
        redirect_to projects_url
      end
    end

    def correct_user_project
      @project = Project.find(params[:project_id])
      if !current_user.projects.ids.include?(@project.id) && !admin? && !current_user.joined_projects.ids.include?(@project.id)
        flash[:warning] = "You are not allowed to access project #{@project.name}"
        redirect_to projects_url
      end
    end

    # Confirm a trainee user
    def trainee_user
      @project = Project.find(params[:project_id])
      unless trainee?
        flash[:warning] = "Just trainee can access that action with report"
        redirect_to project_tasks_path(@project.id)
      end
    end

    def correct_project_report
      @project = Project.find(params[:project_id])
      @task = @project.added_tasks.find(params[:task_id])
      @report = Report.find(params[:id])
      if @report.trainee.joined_projects.pluck(:id).exclude?(@project.id)
        flash[:warning] = "Report doesn't exist or not belongs project #{@project.name}"
        redirect_to project_tasks_url(@project)
      end
    end

    # def correct_url
    #   @project = Project.find(params[:project_id])
    #   @task = @project.added_tasks.find(params[:task_id])
    #   # @report = Report.find(params[:id])
    #   if admin? || trainer?
    #     @users_projects = @project.add_trainees
    #     @users_projects.each do |up|
    #       @users_projects_task = up.add_tasks.find_by(task_id: @task.id)
    #       if @users_projects_task.nil?
    #         next
    #       elsif @users_projects_task.present?
    #         @reports = @users_projects_task.added_reports
    #         if @reports.empty?
    #           flash[:warning] = "No reports in task"
    #           redirect_to project_url(@project) and return
    #         else
    #           # if @reports.where(id: params[:id]).exclude?(@report)
    #           #   flash[:warning] = "No man"
    #           #   redirect_to project_url(@project) and return
    #           # end
    #           # @report = @reports.find(params[:id])
    #           @report = @reports.where(id: params[:id])
    #           break
    #         end
    #       end
    #     end
    #   end
    # end

    def not_started_task_yet
      if trainee?
        @project = Project.find(params[:project_id])
        @task = @project.added_tasks.find(params[:task_id])
        @users_project = current_user.join_projects.find_by(project_id: @project.id)
        @users_projects_task = UsersProjectsTask.find_by(users_project_id: @users_project.id, task_id: @task.id)
        if @users_projects_task.nil?
          flash[:warning] = "You are not started task #{@task.name} yet"
          redirect_to project_tasks_path(@project.id)
        end
      elsif admin? || trainer?
        @project = Project.find(params[:project_id])
        @task = @project.added_tasks.find(params[:task_id])
        @users_projects = @task.belongs_users_projects.where(project_id: @project.id)
        @users_projects.each do |up|
          @users_projects_task = up.add_tasks.find_by(task_id: @task.id)
          if @users_projects_task.nil?
            flash[:warning] = "No trainees have started this task #{@task.name}"
            redirect_to project_tasks_path(@project.id)
          end
        end
      end
    end

    # Confirm correct report can access
    def correct_task_report
      @project = Project.find(params[:project_id])
      @task = @project.added_tasks.find(params[:task_id])
      if trainee?
        @users_project = current_user.join_projects.find_by(project_id: @project.id)
        @users_projects_task = UsersProjectsTask.find_by(users_project_id: @users_project.id, task_id: @task.id)
        @reports = @users_projects_task.added_reports
        @report = Report.find(params[:id])
        if @reports.exclude?(@report)
          flash[:warning] = "Report doesn't exist or is not yours"
          redirect_to project_tasks_path(@project.id)
        end
      end
    end

    # Confirm project include task when projects/id/task/id
    def project_include_task
      @project = Project.find(params[:project_id])
      @task = Task.find(params[:task_id])
      if @project.added_tasks.ids.exclude?(@task.id)
        flash[:warning] = "Task #{@task.name} hasn't been added to project #{@project.name}"
        redirect_to project_tasks_path(@project.id)
      end
    end

    # Confirm correct task can access
    def correct_user_project_task
      if admin?
        @project = Project.find(params[:project_id])
        @task = Task.find(params[:task_id])
        if @project.added_tasks.ids.exclude?(@task.id)
          flash[:warning] = "Add task #{@task.name} to project #{@project.name} to access this task"
          redirect_to project_tasks_path(@project.id)
        end
      elsif trainee?
        @project = Project.find(params[:project_id])
        @task = Task.find(params[:task_id])
        @projects = current_user.joined_projects
        @task_can_view = []
        @projects.each do |p|
          p.added_tasks.each do |t|
            @task_can_view << t.id
          end
        end
        if @task_can_view.exclude?(@task.id)
          flash[:warning] = "You are not allow to access #{@task.name} task"
          redirect_to project_tasks_path(@project.id)
        end
      else trainer?
      @project = Project.find(params[:project_id])
      @task = Task.find(params[:task_id])
      @projects = current_user.projects
      @task_can_view = []
      @projects.each do |p|
        p.added_tasks.each do |t|
          @task_can_view << t.id
        end
      end
      if @task_can_view.exclude?(@task.id)
        flash[:warning] = "Add task #{@task.name} to your project to access this task"
        redirect_to project_tasks_path(@project.id)
      end
      end
    end
  end
end
