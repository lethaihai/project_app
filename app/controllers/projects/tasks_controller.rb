class Projects::TasksController < ApplicationController
  before_action :logged_in_user, only: [:show, :new, :create, :edit, :update, :index, :destroy, :trainees] # write in application_controller.rb
  before_action :dont_have_any_project, only: [:show, :new, :create, :edit, :update, :index, :destroy, :trainees]
  before_action :correct_user_project, only: [:show, :new, :create, :edit, :update, :destroy, :trainees, :index]
  before_action :project_include_task, only: [:show, :trainees]
  before_action :correct_user_project_task, only: [:show, :edit, :update, :destroy, :trainees]
  before_action :admin_or_trainer, only: [:new, :create]
  before_action :admin_user, only: [:edit, :update, :destroy]
  before_action :redirect_to_new_task, only: :new
  before_action :redirect_to_edit_task, only: :edit

  def index
    @project = Project.find(params[:project_id])
    @tasks = @project.added_tasks.where(deleted: false).order(parent_task_id: :asc, created_at: :desc).paginate(:per_page => 15, page: params[:page])
    @project_tasks = ProjectsTask.where(project_id: @project.id)
    if trainee?
      @users_project = current_user.join_projects.find_by(project_id: @project.id)
      @users_projects_task = UsersProjectsTask.new
    end
  end

  def show
    @project = Project.find(params[:project_id])
    @task = @project.added_tasks.find(params[:id])
    if trainee?
      @users_project = current_user.join_projects.find_by(project_id: @project.id)
      @users_projects_task = UsersProjectsTask.find_by(users_project_id: @users_project.id, task_id: @task.id)
    end
  end

  def edit
  end

  def trainees
    @project = Project.find(params[:project_id])
    @task = @project.added_tasks.find(params[:id])
    @users_projects = @task.belongs_users_projects.where(project_id: @project.id).order(status: :asc).paginate(:per_page => 15, page: params[:page])
    @users_projects_tasks = @task.belong_users_projects
  end

  private

  # Allow change attributes on Web
  def task_params
    if admin? || trainer?
      params.require(:task).permit(:name, :duration, :description)
    end
  end

  def dont_have_any_project
    if current_user.projects.empty? && !admin? && current_user.joined_projects.empty?
      flash[:info] = "You don't have or belong to any project"
      redirect_to projects_url
    end
  end

  def correct_user_project
    @project = Project.find(params[:project_id])
    if !current_user.projects.ids.include?(@project.id) && !admin? && !current_user.joined_projects.ids.include?(@project.id)
      flash[:warning] = "You are not allowed to access project #{@project.name}"
      redirect_to projects_url
    end
  end

  # Confirm an admin or trainer
  def admin_or_trainer
    @project = Project.find(params[:project_id])
    unless admin? || trainer?
      flash[:warning] = "Just admin or trainer can access that action with task"
      redirect_to project_tasks_path(@project.id)
    end
  end

  # Confirm an admin user
  def admin_user
    @project = Project.find(params[:project_id])
    unless admin?
      flash[:warning] = "Just admin can access that action with task"
      redirect_to project_tasks_path(@project.id)
    end
  end

  # Redirect to task/new when project/id/task/new
  def redirect_to_new_task
    redirect_to new_task_path
  end

  # Redirect to task/id/edit when project/id/task/id/edit
  def redirect_to_edit_task
    @task = Task.find(params[:id])
    redirect_to edit_task_path(@task)
  end

  # Confirm project include task when projects/id/task/id
  def project_include_task
    @project = Project.find(params[:project_id])
    @task = Task.find(params[:id])
    if @project.added_tasks.ids.exclude?(@task.id)
      flash[:warning] = "Task #{@task.name} hasn't been added to project #{@project.name}"
      redirect_to project_tasks_path(@project.id)
    end
  end

  # Confirm correct user project task
  def correct_user_project_task
    if admin?
      @project = Project.find(params[:project_id])
      @task = Task.find(params[:id])
      if @project.added_tasks.ids.exclude?(@task.id)
        flash[:warning] = "Add task #{@task.name} to project #{@project.name} to access this task"
        redirect_to project_tasks_path(@project.id)
      end
    elsif trainee?
      @project = Project.find(params[:project_id])
      @task = Task.find(params[:id])
      @projects = current_user.joined_projects
      @task_can_view = []
      @projects.each do |p|
        p.added_tasks.each do |t|
          @task_can_view << t.id
        end
      end
      if @task_can_view.exclude?(@task.id)
        flash[:warning] = "You are not allow to access task #{@task.name}"
        redirect_to project_tasks_path(@project.id)
      end
    else trainer?
    @project = Project.find(params[:project_id])
    @task = Task.find(params[:id])
    @projects = current_user.projects
    @task_can_view = []
    @projects.each do |p|
      p.added_tasks.each do |t|
        @task_can_view << t.id
      end
    end
    if @task_can_view.exclude?(@task.id)
      flash[:warning] = "Add task #{@task.name} to project #{@project.name} to access this task"
      redirect_to project_tasks_path(@project.id)
    end
    end
  end
end
