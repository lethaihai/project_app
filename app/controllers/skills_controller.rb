class SkillsController < ApplicationController
  before_action :logged_in_user, only: [:show, :new, :create, :edit, :update, :index, :destroy] # write in application_controller.rb
  before_action :admin_or_trainer, only: [:show, :new, :create]
  before_action :admin_user, only: [:index, :destroy]
  before_action :redirect_when_edit, only: [:edit, :update]

  def show
    @skill = Skill.find(params[:id])
  end

  def new
    @skill = Skill.new
  end

  def create
    @skill = Skill.new(skill_params)
    if @skill.save
      flash[:success] = "Create skill successful"
      if admin?
        redirect_to @skill
      elsif trainer?
        redirect_to edit_user_path(current_user)
      end

    else
      render 'new'
    end
  end

  def edit
  end

  def update
  end

  def index
    @search = Skill.all.ransack(params[:q])
    @skills = @search.result(distinct: true).paginate(:per_page => 10, page: params[:page])
  end

  def destroy
    @skill = Skill.find(params[:id])
    @skill.destroy
    flash[:success] = "Skill #{@skill.name} has been deleted"
    redirect_to skills_path
  end

  private

  def skill_params
    params.require(:skill).permit(:name)
  end

  # Confirm an admin or trainer
  def admin_or_trainer
    unless admin? || trainer?
      flash[:warning] = "Just admin or trainer can access that action with skill"
      redirect_to root_path
    end
  end

  # Confirm an admin user
  def admin_user
    unless admin?
      flash[:warning] = "Just admin can access that action with skill"
      redirect_to root_path
    end
  end

  # Redirect went /skills/.../edit
  def redirect_when_edit
    @skill = Skill.find(params[:id])
    redirect_to @skill
  end
end