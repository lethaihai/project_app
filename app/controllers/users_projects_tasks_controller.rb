class UsersProjectsTasksController < ApplicationController
  before_action :logged_in_user, only: [:show, :new, :create, :edit, :update, :index, :destroy, :trainees, :tasks] # write in application_controller.rb

  def new
    @users_projects_task = UsersProjectsTask.new
  end

  def create
    @users_projects_task = UsersProjectsTask.new(users_projects_task_params)
    if @users_projects_task.save
      @project = @users_projects_task.users_project.project
      flash[:success] = "Success"
      redirect_to project_tasks_path(@project)
    else
      flash[:danger] = "Failed"
      redirect_to projects_path
    end
  end

  def edit
    @users_projects_task = UsersProjectsTask.find(params[:id])
  end

  def update
    @users_projects_task = UsersProjectsTask.find(params[:id])
    if @users_projects_task.update_attributes(users_projects_task_params)
      @project = @users_projects_task.users_project.project
      flash[:success] = "Success"
      redirect_to project_tasks_path(@project)
    else
      flash[:danger] = "Failed"
      redirect_to projects_path
    end
  end

  private

  # Allow change attributes on Web
  def users_projects_task_params
    params.permit(:users_project_id, :task_id, :started, :finished, :status)
  end

end
